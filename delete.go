package redis

// Delete deletes entry
func Delete(key string) error {
	conn := Pool.Get()
	if _, err := conn.Do("DEL", key); err != nil {
		return err
	}
	return nil
}
