package redis

// Get  gets data from redis cache and sets data
func Get(key string, data Data) error {
	conn := Pool.Get()
	reply, err := conn.Do("GET", key)
	if err != nil {
		return err
	}

	return data.Parse(reply)
}
