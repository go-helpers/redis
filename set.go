package redis

//Set set data
func Set(input Data) error {
	conn := Pool.Get()

	conn.Send("MULTI")
	conn.Send("SET", input.Key(), input.Data())
	if expiry := input.Expires(); expiry > 0 {
		conn.Send("EXPIRE", input.Key(), expiry)
	}
	if _, err := conn.Do("EXEC"); err != nil {
		return err
	}
	return nil
}
