package redis

import (
	"time"

	redigo "github.com/gomodule/redigo/redis"
)

// Data ..
type Data interface {
	Key() string
	Data() interface{}
	Expires() int64
	Parse(interface{}) error
}

// Pool redigo pool init
var Pool *redigo.Pool

// InitPool returns a redis pool
/*
@params endpoint redis://endpoint
*/
func InitPool(endpoint string, maxIdle int, idleTimeout time.Duration) error {
	var err error
	Pool = &redigo.Pool{
		MaxIdle:     maxIdle,
		IdleTimeout: idleTimeout * time.Second,
		Dial: func() (redigo.Conn, error) {
			var c redigo.Conn
			if c, err = redigo.DialURL(endpoint); err != nil {
				return nil, err
			}
			return c, err
		},
	}
	return err
}
