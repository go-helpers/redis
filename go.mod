module gitlab.com/go-helpers/redis

go 1.12

require (
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/rafaeljusto/redigomock v2.4.0+incompatible
	github.com/stretchr/testify v1.6.1
)
