package redis_test

import (
	"fmt"
	"testing"

	redigo "github.com/gomodule/redigo/redis"
	"github.com/rafaeljusto/redigomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/go-helpers/redis"
)

var (
	poolMock      *redigo.Pool
	mockRedisConn *redigomock.Conn
)

func init() {
	mockRedisConn = redigomock.NewConn()
	redis.Pool = redigo.NewPool(func() (redigo.Conn, error) {
		return mockRedisConn, nil
	}, 10)
}

type TestData struct{}

func (me *TestData) Key() string {
	return "testdata"
}

func (me *TestData) Data() interface{} {
	return []byte("testdata")
}

func (me *TestData) Expires() int64 {
	return 100
}
func (me *TestData) Parse(b interface{}) error {
	fmt.Println("testdata.Parse", b)
	return nil
}

func Test_Set(t *testing.T) {
	td := new(TestData)
	fmt.Println(td)

	mockRedisConn.Clear()
	mockRedisConn.Command("MULTI")
	mockRedisConn.Command("SET", td.Key(), td.Data())
	mockRedisConn.Command("EXPIRE", td.Key(), td.Expires())
	mockRedisConn.Command("EXEC")

	assert.Nil(t, redis.Set(td))
	assert.NoError(t, mockRedisConn.ExpectationsWereMet())
}

func Test_Get(t *testing.T) {
	td := new(TestData)
	mockRedisConn.Clear()
	mockRedisConn.Command("GET", td.Key()).Expect(td.Data())

	assert.Nil(t, redis.Get(td.Key(), td))
	assert.NoError(t, mockRedisConn.ExpectationsWereMet())
}

func Test_Delete(t *testing.T) {
	mockRedisConn.Clear()
	mockRedisConn.Command("DEL", "key").Expect(int64(1))
	err := redis.Delete("key")
	assert.Nil(t, err)
	assert.NoError(t, mockRedisConn.ExpectationsWereMet())
}
